const gulp = require("gulp");
const ui5preload = require("gulp-ui5-preload");
const prettydata = require("gulp-pretty-data");
const gulpif = require("gulp-if");
const clean = require("gulp-clean");
const uglify = require("gulp-uglify-es").default;
const { join, relative } = require("path");

const Configurator = require("./src/modules/engine/helpers/Configurator");
const configurator = new Configurator();
configurator.loadModules();
const modules = configurator.getModules();

gulp.task("ui5preload", function (cb) {
  modules.forEach(currModule => {
    const relSrcModule = relative(__dirname, currModule.src);
    return gulp
      .src([
        join(relSrcModule, "../webapp/**/**.+(js|xml)"),
        `!${join(relSrcModule, "../webapp/**/Component-preload.js")}`,
      ])
      .pipe(gulpif("**/*.js", uglify()))
      .pipe(gulpif("**/*.xml", prettydata({ type: "minify" })))
      .pipe(
        ui5preload({
          base: `${join(relSrcModule, '..')}/webapp`,
          namespace: `engine.${currModule._id}`
        })
      )
      .pipe(gulp.dest(join(relSrcModule, "../webapp")));
  });

  cb();
});

gulp.task("clearui5preload", function (cb) {
  modules.forEach(currModule => {
    return gulp
      .src(join(currModule.src, "../webapp/Component-preload.js"), {
        read: false,
        allowEmpty: true
      })
      .pipe(clean());
  });

  cb();
});
