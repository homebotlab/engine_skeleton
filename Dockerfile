FROM node:10-alpine
RUN mkdir /app
COPY . /app
WORKDIR /app
EXPOSE 6001
CMD [ "npm", "run", "dev" ]