const fs = require("fs");
const { promisify } = require("util");
const readFileAsync = promisify(fs.readFile);
const AccessControl = require("accesscontrol");

module.exports = (on, config) => {
  on("task", {
    manifestDataByPath(path) {
      return readFileAsync(path, {
        encoding: "utf-8"
      }).then(data => {
        const manifestData = JSON.parse(data);
        return manifestData;
      });
    },

    isGrantedAccess({ grantList, role, action, resource }) {
      const ac = new AccessControl(grantList);
      const permission = ac.can(role)[action](resource);

      return permission.granted;
    }
  });
};
