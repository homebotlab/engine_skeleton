/*global location*/
sap.ui.define(
  [
    'engine/profile/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'sap/ui/core/routing/History',
    'engine/profile/model/formatter',
    'engine/profile/model/models',
    'sap/m/MessageToast'
  ],
  function(BaseController, JSONModel, History, formatter, Model, MessageToast) {
    'use strict'

    return BaseController.extend('engine.profile.controller.Object', {
      formatter: formatter,

      onInit: function() {
        var iOriginalBusyDelay,
          oViewModel = new JSONModel({
            busy: true,
            delay: 0,
            isNew: null
          })
        this.getRouter()
          .getRoute('object')
          .attachPatternMatched(this._onObjectMatched, this)

        iOriginalBusyDelay = this.getView().getBusyIndicatorDelay()
        this.setModel(oViewModel, 'objectView')

        // set data model
        this.setModel(new JSONModel())
                
        Model.loadRoles().done(function(oResponse) {
          const i18nRoles = oResponse.data.roles.map(role => {
            return {
              role,
              i18nRole: this.getResourceBundle().getText(role)
            }
          });
           
          this.getModel('objectView').setProperty('/roles', i18nRoles);
        }.bind(this));

        Model.loadRegions().done(
          function(response) {
            this.getModel('objectView').setProperty(
              '/regionList',
              response
            )
          }.bind(this)
        )
      },

      onNavBack: function() {
        var sPreviousHash = History.getInstance().getPreviousHash()

        if (sPreviousHash !== undefined) {
          history.go(-1)
        } else {
          window.location = '/modules/launchpad/webapp/index.html';
        }
      },

      handleEditPress: function() {
        //Clone the data
        this._oSupplier = jQuery.extend(
          {},
          this.getView()
            .getModel()
            .getData().SupplierCollection[0]
        )
        this._toggleButtonsAndView(true)
      },

      handleCancelPress: function() {
        //Restore the data
        var oModel = this.getView().getModel()
        var oData = oModel.getData()

        oData.SupplierCollection[0] = this._oSupplier

        oModel.setData(oData)
        this._toggleButtonsAndView(false)
      },

      onSavePress: function() {
        this._save()

        // TODO
        //this._toggleButtonsAndView(false);
      },

      onDeletePress: function() {
        this._deleteUser()
      },

      /* =========================================================== */
      /* internal methods                                            */
      /* =========================================================== */

      _formFragments: {},

      _toggleButtonsAndView: function(bEdit) {
        var oView = this.getView()
        // Show the appropriate action buttons
        oView.byId('edit').setVisible(!bEdit)
        oView.byId('save').setVisible(bEdit)
        oView.byId('cancel').setVisible(bEdit)

        // Set the right form type
        this._showFormFragment(bEdit ? 'Change' : 'Display')
      },

      _getFormFragment: function(sFragmentName) {
        var oFormFragment = this._formFragments[sFragmentName],
            oFragmentController;

        if (oFormFragment) {
          return oFormFragment
        }

        /**
         * attaching fragment to either it's separate controller or to this controller
         */
        try {
          oFragmentController = sap.ui.controller('engine.profile.controller.object.' + sFragmentName);
        } catch (oErr) {
          oFragmentController = this;
        }

        oFormFragment = sap.ui.xmlfragment(
          this.getView().getId(),
          'engine.profile.view.object.' + sFragmentName,
          oFragmentController
        )

        this._formFragments[sFragmentName] = oFormFragment
        return this._formFragments[sFragmentName]
      },

      _showFormFragment: function(sFragmentName) {
        var oPage = this.getView().byId('page')

        oPage.removeAllContent()
        oPage.insertContent(this._getFormFragment(sFragmentName))
      },

      /**
       * Binds the view to the object path.
       * @function
       * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
       * @private
       */
      _onObjectMatched: function(oEvent) {
        var sObjectId = oEvent.getParameter('arguments').objectId
        var isNew = false

        if (sObjectId == '0') {
          this.getModel().setProperty('/', {
            role: {_id: ''},
            email: '',
            firstname: '',
            middlename: '',
            lastname: '',
            password: '',
            status: '0',
            group: '0',
            username: ''
          })
          this.getView().bindElement({ path: '/' })
          this._onBindingChange()

          isNew = true
        } else {
          this._load(sObjectId)
        }

        this.getModel('objectView').setProperty('/isNew', isNew)
      },

      _save: function() {
        var isNew = this.getModel('objectView').getProperty('/isNew')
        
        var oData = $.extend({}, this.getModel().getProperty('/'))
        var entityName = this.getOwnerComponent().getObjectEntitySetName()

        $.ajax({
          url: isNew ? entityName : entityName + '/' + oData._id,
          method: isNew ? 'POST' : 'PUT',
          contentType: 'application/json',
          data: JSON.stringify(oData)
        })
          .done(
            function(data) {
              MessageToast.show('Данные сохранет успешно.')

              if (this.getModel('objectView').getProperty('/isNew')) {
                var objectPropertyName = this.getOwnerComponent().getObjectPropertyName()
                this.getRouter().navTo(
                  'object',
                  {
                    objectId: data.d.results[objectPropertyName]
                  },
                  true
                )
              }
            }.bind(this)
          )
          .fail(
            function(jqXHR, text) {
              this.getOwnerComponent()
                .getErrorHandler()
                .showServiceError(jqXHR.responseText)
            }.bind(this)
          )
      },

      _deleteUser: function() {
        var objectEntitySetName = this.getOwnerComponent().getObjectEntitySetName()
        var objectPropertyName = this.getOwnerComponent().getObjectPropertyName()
        var url =
          objectEntitySetName +
          '/' +
          this.getModel().getProperty('/' + objectPropertyName)

        $.ajax(url, {
          method: 'DELETE',
          dataType: 'json',
          success: function(res) {
            MessageToast.show('Данные сохранены успешно')
            this.onNavBack()
          }.bind(this)
        })
      },

      _load: function(id) {
        var objectEntitySetName = this.getOwnerComponent().getObjectEntitySetName()
        $.getJSON(
          objectEntitySetName + '/' + id,
          function(response) {
            this.getModel().setProperty('/', {
              ...response,
              regions: response.regions.map(region => region._id)
            });
            this.getView().bindElement({ path: '/' })
            this._onBindingChange()
          }.bind(this)
        )
      },

      _onBindingChange: function() {
        var oView = this.getView(),
          oViewModel = this.getModel('objectView'),
          oElementBinding = oView.getElementBinding()

        // No data for the binding
        if (!oElementBinding.getBoundContext()) {
          this.getRouter()
            .getTargets()
            .display('objectNotFound')
          return
        }

        // Everything went fine.
        oViewModel.setProperty('/busy', false)

        // Set the initial form to be the display one
        //this._showFormFragment("Display"); // TODO:
        this._showFormFragment('Change')
      },

      onExit: function() {
        for (var sPropertyName in this._formFragments) {
          if (!this._formFragments.hasOwnProperty(sPropertyName)) {
            return
          }

          this._formFragments[sPropertyName].destroy()
          this._formFragments[sPropertyName] = null
        }
      }
    })
  }
)
