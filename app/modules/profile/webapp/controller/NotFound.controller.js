sap.ui.define(['engine/profile/controller/BaseController'], function(
  BaseController
) {
  'use strict'

  return BaseController.extend('engine.profile.controller.NotFound', {
    /**
     * Navigates to the worklist when the link is pressed
     * @public
     */
    onLinkPressed: function() {
      this.getRouter().navTo('worklist')
    }
  })
})
