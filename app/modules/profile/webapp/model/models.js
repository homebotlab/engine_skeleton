sap.ui.define(['sap/ui/model/json/JSONModel', 'sap/ui/Device'], function(
  JSONModel,
  Device
) {
  'use strict'

  return {
    createDeviceModel: function() {
		var oModel = new JSONModel(Device)
		oModel.setDefaultBindingMode('OneWay')
		return oModel
    },

    loadRoles: function() {
			return jQuery.ajax({
				url: '/api/v0.1/configurator/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
					query: `
					{
						roles
					}`
			})
    })
	},
	
	loadRegions: function() {
		return $.getJSON('/api/v0.1/forms/regions')
	}
  }
})
