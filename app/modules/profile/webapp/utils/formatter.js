sap.ui.define("engine.profile.utils.formatter", ['sap/ui/core/format/DateFormat'], function (DateFormat) {
	"use strict";
	return {
		stringDateParse:function(sDate){
			return new Date(Date.parse(sDate))
		},

		dateFormatString:function(sDate, sPattern){
			return DateFormat.getDateInstance({pattern: sPattern || 'dd.MM.yyyy HH-mm'}).format(new Date(Date.parse(sDate)))
		},

		isVisibleNew: function(bIsNew)
		{
			return !bIsNew
		}
	};
}, true);