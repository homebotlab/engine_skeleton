sap.ui.define("engine.events.utils.filterController", [
    'sap/ui/core/mvc/Controller',
    'jquery.sap.global',
    'sap/ui/model/json/JSONModel'
], function(Controller, jQuery, JSONModel){
    'use strict';
    return Controller.extend("engine.events.utils.filterController", {
        
        _serviceModel: new JSONModel(),
        /**
         * creates a dialog from fragment;
         * uses either a specified path in the fragPath property of the controller 
         * or attempts to find a fragment in a similar to controller directory structure
         */
        openFilterDialog: function(oEvent){
            if(!this._filterDialog && !!this._serviceModel)
            {   
                var sDefaultPath = this.getMetadata().getName().slice(0, this.getMetadata().getName().lastIndexOf(".") + 1),
                    sfragPath = this._serviceModel.getProperty("/fragPath") || sDefaultPath.replace("controller", "view") + "filterDialog";
                //checking if there was a correct path retrieval
                if(sfragPath !== sDefaultPath)
                {
                    this._filterDialog = sap.ui.xmlfragment("engine.events.utils.filterDialog", this).addContent(sap.ui.xmlfragment(sfragPath));
                    //setting initial data
                    this.initFilters();
                }
            }
            this._filterDialog._deffered = jQuery.Deferred();
            this._filterDialog.open();
        },

        closeFilterDialog: function(){
            this._filterDialog && this._filterDialog.close();
        },

        /**
         * gathers all available defined (not null and empty) filters and sends them in a GET-request to server
         * receives data back and passes it to deffered handlers
         */
        applyFilters: function(oEvent, fnFilterProcess){
            var sURL = this._serviceModel.getProperty("/dataURL");
            if(this._serviceModel && sURL)
            {
                this._filterDialog.setBusy(true);
                var that = this,
                    oData = jQuery.extend({}, this._filterDialog.getModel("filters").getData()),
                    vRes = "";
                if(fnFilterProcess)
                {
                    oData = fnFilterProcess(oData);
                    vRes = "filter=";
                }
                //will only send defined/changed model properties
                var oFilt = Object.keys(oData).reduce(function(oTemp, sProp){
                    //filters out simple types
                    if(oData[sProp] !== null && oData[sProp] !== undefined && oData[sProp] !== "")
                    {
                        //filters out empty object and arrays
                        if(typeof oData[sProp] === 'object' && jQuery.isEmptyObject(oData[sProp]) && !(oData[sProp] instanceof Date))
                            return oTemp
                        oTemp[sProp] = oData[sProp];
                    }
                    return oTemp
                }, {});
                vRes += JSON.stringify(oFilt);
                //will not use handler if it's not passed as an argument or filtered object is empty
                jQuery.get(sURL, jQuery.isEmptyObject(oFilt) ? "" : vRes).done(function(oRes){
                    that._filterDialog.setBusy(false).close()._deffered.resolve(oRes);
                }).fail(function(oErr){
                    that._filterDialog.setBusy(false).close()._deffered.reject(oErr);
                });
            }
        },

        /**
         * inits or resets filter model to default values
         */
        initFilters: function(){
            var oFilters = this._filterDialog.getModel("filters"),
                //extending to create a new object rather than using the same one for both models
                oDefFilters = jQuery.extend({}, this._serviceModel.getProperty("/defFilterData"));
            if(oFilters && this._filterDialog.getModel("dicts"))
            {
                oFilters.setData(oDefFilters);
            }
            else
            {
                //initialization
                this._filterDialog.setModel(new JSONModel(this._serviceModel.getProperty("/dicts")), "dicts");
                this._filterDialog.setModel(new JSONModel(oDefFilters), "filters");
            }
        },

        /**
         * TODO use this method to load filter dictionaries from server
         */
        _loadFilterDicts: function(){

        }
    });
}, true);