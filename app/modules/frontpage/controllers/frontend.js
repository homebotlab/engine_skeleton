const svgCaptcha = require('svg-captcha');

exports.index = function (req, res, next) {
    const errors = req.flash('error');

    if (req.body.target) {
        console.log(req.session.target)
    }

    res.render('pages/index', {
        errors
    });
}

exports.login = function (req, res, next) {
    const errors = req.flash('error');
    if (req.body.target) {
        console.log(req.session.target)
    }

    res.render('pages/login', {
        errors
    });
}

exports.register = async function (req, res) {
    const success = req.flash('success');
    const errors = req.flash('error');

    if (req.body.target) {
        console.log(req.session.target)
    }

    res.render('pages/register', {
        errors,
        success
    });
}

exports.approveEmail = async function (req, res) {
    const success = req.flash('success');
    const errors = req.flash('error');

    if (req.body.target) {
        console.log(req.session.target)
    }

    res.render('pages/approveEmail', {
        errors,
        success
    });
}

exports.recoveryPass = async function (req, res) {
    const errors = req.flash('error');
    const success = req.flash('success');

    const captcha = svgCaptcha.create();
    req.session.captcha = captcha.text;

    res.render('pages/recoveryPass', {
        captcha: captcha.data,
        errors, 
        success
    });
}

exports.changePass = async function (req, res) {
    const errors = req.flash('error');
    const success = req.flash('success');

    res.render('pages/changePass', {
        errors, success
    });
}