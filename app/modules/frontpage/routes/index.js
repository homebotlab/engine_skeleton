const path = require('path');
const express = require('express');
const frontend = require('../controllers/frontend');

const routes  = express.Router();

routes.use((req, res, next) => {
  req.app.set('views', path.resolve(__dirname, '../views'));
  req.app.set('view engine', 'twig');
  req.app.set('view cache', false);
  next();
});

routes.route('/')
  .get(frontend.index)

routes.route('/login')
  .get(frontend.login)

routes.route('/approve-email')
  .get(frontend.approveEmail)

routes.route('/register')
  .get(frontend.register)

routes.route('/recovery-password')
  .get(frontend.recoveryPass)

routes.route('/change-password')
  .get(frontend.changePass)

module.exports = routes;
