sap.ui.define(
  [
    'engine/forms/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'engine/forms/model/formatter',
    'sap/m/MessageToast'
  ],
  function(BaseController, JSONModel, formatter, MessageToast) {
    'use strict'

    return BaseController.extend('engine.forms.controller.App', {
      formatter: formatter,

      onInit: function() {
        this.setModel(new JSONModel())

        this.setModel(
          new JSONModel({
            pageTitle: 'Форма - ' + this.getOwnerComponent().getFormId(),
            regions: [],
            region_id: null,
            year: '2017',
            form_id: this.getOwnerComponent().getFormId()
          }),
          'objectView'
        )

        this.getView().bindElement({ path: '/' })

        $.getJSON(
          '/users/regions',
          function(response) {
            this.getModel('objectView').setProperty(
              '/regions',
              response.d.results
            )
            this.getModel('objectView').setProperty(
              '/region_id',
              response.d.results[0].id
            )
            this._loadFormSet()
          }.bind(this)
        )
      },

      onAfterRendering: function() {
        if (!this.oForm) {
          var sFormId = this.getOwnerComponent().getFormId()
          this.oForm = sap.ui.xmlfragment(
            this.getView().getId(),
            'engine.forms.view.forms.' + sFormId
          )
          this.byId('page').addContent(this.oForm)
        }
      },

      /**
       * Event handler  for navigating back.
       * It there is a history entry we go one step back in the browser history
       * If not, it will replace the current entry of the browser history with the worklist route.
       * @public
       */
      onNavBack: function() {
        history.go(-1)
      },

      _loadFormSet: function() {
        var filters = {
          region_id: this.getModel('objectView').getProperty('/region_id'),
          year: this.getModel('objectView').getProperty('/year'),
          form_id: this.getModel('objectView').getProperty('/form_id')
        }

        $.getJSON(
          '/forms/form',
          filters,
          function(response) {
            this.getModel().setProperty('/', response.d.results[0])
          }.bind(this)
        )
      },

      onChangeRFSubjectID: function() {
        this._loadFormSet()
      },

      onChangeData: function() {
        this._loadFormSet()
      },

      onSave: function() {
        var oData = $.extend(
          {
            form_id: this.getModel('objectView').getProperty('/form_id'),
            region_id: this.getModel('objectView').getProperty('/region_id'),
            year: this.getModel('objectView').getProperty('/year')
          },
          this.getModel().getProperty('/')
        )

        $.post('/forms/form', oData)
          .done(function(data) {
            MessageToast.show('Данные сохранет успешно.')
          })
          .fail(
            function(jqXHR, text) {
              this.getOwnerComponent()
                .getErrorHandler()
                .showServiceError(jqXHR.responseJSON)
            }.bind(this)
          )
      }

      // onExit : function () {
      //     if (this._oPopover) {
      //         this._oPopover.destroy();
      //     }
      // },

      // onPopoverPress: function (oEvent) {

      //     // create popover
      //     if (!this._oPopover) {
      //         this._oPopover = sap.ui.xmlfragment("engine.forms.view.InfoPopover", this);
      //         this.getView().addDependent(this._oPopover);
      //         this._oPopover.bindElement(oEvent.getSource().data("path"));
      //     }

      //     this._oPopover.openBy(oEvent.getSource());
      // }
    })
  }
)
