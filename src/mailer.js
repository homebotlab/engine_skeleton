// TODO move to module "mailer"
const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');

// TODO get config from json
const smtpConfig = {
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  secure: false
};

if(process.env.EMAIL_AUTH){
  smtpConfig.auth = {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS
  }
}

const transporter = nodemailer.createTransport(smtpConfig);

transporter.use('compile', hbs({
  viewEngine: {
    extname: '.hbs',
    partialsDir: 'src/modules/',
    defaultLayout: false
  },
  viewPath: 'src/modules/',
  extName: '.hbs'
}));

module.exports = transporter;
