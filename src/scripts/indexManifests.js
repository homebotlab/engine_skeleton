const nodegit = require("nodegit");
const path = require("path");
const modules = require("../helpers/modules").getModules();

(async () =>{
    const repo = await nodegit.Repository.open(path.resolve(__dirname, "../../.git"));
    const index = await repo.refreshIndex();

    try {
        const manifestFiles = modules.map(item => item.src.replace(/\\/g, '/'));
        for (const manifestFile of manifestFiles) {
            await index.addByPath(manifestFile);
        }
        await index.write();
        await index.writeTree();

        console.log('Manifest files are indexed.')      
        
    } catch (error) {
        console.log('Indexing error:', error);
    } 
})();
