const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

require("dotenv").config();

require("../modules/engine_users/models/user");
require("../modules/engine/db");

mongoose.connect(process.env.DATABASE, {
  useNewUrlParser: true,
  useCreateIndex: true
});

const User = mongoose.model("User");

(async function() {

  await User.updateOne(
    {
      email: process.env.ADMIN_EMAIL
    },
    {
      username: process.env.ADMIN_USERNAME,
      password: hashPass(process.env.ADMIN_PASSWORD),
      email: process.env.ADMIN_EMAIL,
      role: "admin",
      status: true,
      isEmailApproved: true
    },
    {
      upsert: true
    }
  );

  console.log("Admin loaded into DB");
  process.exit();
})();

function hashPass(password) {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);

  return hash;
}
