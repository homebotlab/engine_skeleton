const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const log = require('log4js').getLogger("passport");
const modules = require(`${process.env.HELPERS_PATH}/modules`);
require(modules.getModel("User"));

const User = mongoose.model('User');

module.exports = function (passport) {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (obj, done) {
    done(null, obj);
  });

  const LocalStrategy = require('passport-local').Strategy;

  passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, (req, email, password, done) => {

    User.findOne({
      email
    }, (err, user) => {

      if (err) {
        log.error('Unexpected mongoose error', err);
        return done(err);
      }

      if (user) {

        if (!user.isEmailApproved) {
          log.warn(`Login attempt without verified email by a user with email ${user.email}`);

          return done(null, false, req.flash('error', {
            message: 'У пользователя не подтверждена почта'
          }));     
        } 

        if (!user.status) {
          log.warn(`Login attempt without active status by a user with email ${user.email}`);

          return done(null, false, req.flash('error', {
            message: 'Пользователь не активирован'
          }));
        }

        bcrypt.compare(password, user.password, (err, isValid) => {
          if (err) {
            log.error('Unexpected bcrypt error:', err);
            return done(err);
          }

          if (!isValid) {
            log.warn(`Invalid password entered by a user with email ${user.email}`);
            return done(null, false, req.flash('error', {
              message: 'Указан не верный пароль'
            }));
          }
          return done(null, user)
        });
        
      } else {
        log.warn(`Login attempt by email ${email}. Such user does not exist.`);
        return done(null, false, req.flash('error', {
          message: 'Пользователь с указанным email не найден'
        }));
      }
    });
  }));
};